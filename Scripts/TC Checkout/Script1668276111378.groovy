import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('TC Checkout Block/TC Login - block'), [('username') : 'bob@example.com', ('password') : '10203040'
        , ('is_error') : false, ('error_message') : ''], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('TC Checkout Block/TC Add to Cart - block'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('checkout screen/btn-proceed-checkout'), 0)

Mobile.waitForElementPresent(findTestObject('checkout screen/txt-Checkout'), 0)

Mobile.setText(findTestObject('checkout screen/txtfield-fullname'), fullname, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-address-1'), address_line_1, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-address-2'), address_line_2, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-city'), city, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-state'), state, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-zip-code'), zip_code, 0)

Mobile.setText(findTestObject('checkout screen/txtfield-country'), country, 0)

Mobile.tap(findTestObject('checkout screen/btn-to-payment'), 0)

Mobile.waitForElementPresent(findTestObject('checkout screen/card/txtfield-fullname'), 4)

Mobile.setText(findTestObject('checkout screen/card/txtfield-fullname'), card_fullname, 0)

Mobile.setText(findTestObject('checkout screen/card/txtfield-card-number'), card_number, 0)

Mobile.setText(findTestObject('checkout screen/card/txtfield-expr-date'), expr_date, 0)

Mobile.setText(findTestObject('checkout screen/card/txtfield-security-code'), sec_code, 0)

Mobile.tap(findTestObject('checkout screen/card/btn-review-order'), 0)

Mobile.tap(findTestObject('checkout screen/card/btn-review-order'), 0)

Mobile.waitForElementPresent(findTestObject('checkout screen/review order/btn-place-order'), 0)

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-address-fullname'), fullname)

String address_full = (address_line_1 + ', ') + address_line_2

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-address1, address2'), address_full)

String city_state = (city + ', ') + state

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-city, state'), city_state)

String country_zip = (country + ', ') + zip_code

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-country, zip-code'), country_zip)

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-card-fullname'), card_fullname)

card_number = card_number.replaceAll("(.{4})", '$1 ').trim()

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-card-number'), card_number)

String formatted_expr_date = 'Exp: ' + formatExpr(expr_date)

def formatExpr(String date) {
	return date.substring(0,2) + '/' + date.substring(2)
}

Mobile.verifyElementText(findTestObject('checkout screen/review order/txt-Exp'), formatted_expr_date)

Mobile.tap(findTestObject('checkout screen/review order/btn-place-order'), 0)

Mobile.waitForElementPresent(findTestObject('checkout screen/complete screen/txt - Checkout Complete'), 0)

Mobile.callTestCase(findTestCase('TC Checkout Block/TC Logout - block'), [:], FailureHandling.STOP_ON_FAILURE)

