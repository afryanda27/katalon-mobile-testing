import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver

Mobile.startExistingApplication(GlobalVariable.APP_ID, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('add to cart/btn-item'), 0)

String objectSubFolder = 'add to cart/color/'

String pathToObjRep = (RunConfiguration.getProjectDir() + '/Object Repository/') + objectSubFolder

File yourObjects = new File(pathToObjRep)

// get all files from the directory
String[] fileNames = yourObjects.list()

List<String> testObjects = new ArrayList()

// get only test objects, not other subdirectories if there are any (with .rs extension)
for (int i = 0; i < fileNames.length; i++) {
    if ((fileNames[i]).contains('.rs')) {
        (fileNames[i]) = (fileNames[i]).replace('.rs', '')

        // add to a list a relative path to TestObject within Object Repository
        testObjects.add(objectSubFolder + (fileNames[i]))
    }
}

// foreach test object, do anything you want
for (String obj : testObjects) {
	
    Mobile.tap(findTestObject(obj), 0)

    Mobile.tap(findTestObject('add to cart/btn-add-to-cart'), 0)

    Mobile.tap(findTestObject('add to cart/btn-my-cart'), 0)

    Mobile.waitForElementPresent(findTestObject('add to cart/txt-Sauce Labs Backpack-in-cart'), 5)
	
	Mobile.pressBack()
}

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.APP_ID)



